(ns clojure-x86-interpreter.memory-instructions)
(require '[clojure-x86-interpreter.program-state :refer (set-reg)])
(require '[clojure-x86-interpreter.utils :refer (extract-bits)])

(defn mov
  "mov src dst"
  [src dst]
  (set-reg dst src))

(defn movb
  "movb imm reg"
  [imm reg]
  (set-reg reg (extract-bits imm 8 0)))