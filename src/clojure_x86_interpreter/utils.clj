(ns clojure-x86-interpreter.utils)

(defn append-bit-vector
  "appends to bitvector new element"
  [bitvector bit]
  (if (or (== bit 0) (== bit 1))
    (conj bitvector bit)
    (println "Trying to pass" bit "to bitvector\n")))

(defn dec-to-bin [decimal]
  (loop [n decimal
         res (vector)]
    (if (= n 0)
      res
      (recur (quot n 2)
             (append-bit-vector res (mod n 2))))))

(defn parse-float [s]
  (unchecked-long (read-string s)))

;return (((1 << size) - 1) & (number >> (start)));
(defn extract-bits [number size start]
  (if (= size 64)
    number
    (bit-and (- (bit-shift-left 1 size) 1) (bit-shift-right number start)))
  )

(defn get-bit-width
  [reg]
  (let [reg-name (name reg)
        total-count (count reg-name)
        {first-char 0, last-char (- total-count 1)} reg-name]
    (cond
      (= last-char \h) [8 8 0xFFFFFFFFFFFF00FF]
      (or (= last-char \b) (= last-char \l)) [8 0 0xFFFFFFFFFFFFFF00]
      (or (= last-char \w) (and (= total-count 2) (not= first-char \r))) [16 0 0xFFFFFFFFFFFF0000]
      (or (= last-char \d) (= first-char \e)) [32 0 0xFFFFFFFF00000000]
      (= first-char \r) [64 0 0]
      :else [0 0 0XF]
      )))
