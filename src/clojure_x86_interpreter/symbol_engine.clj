(ns clojure-x86-interpreter.symbol-engine)

(defrecord
  "name: s1, s2, s3
  path-predicate: Z3 constraints
  address: offset in memory
  size: length of symbol
  type: int, char, float"
  symbol-type [name
               path-predicate
               address
               size
               type])
