(ns clojure-x86-interpreter.program-state)
(require '[clojure-x86-interpreter.utils :refer (parse-float extract-bits get-bit-width)])
(require '[clojure-x86-interpreter.memory-state :refer :all])
(require '[clojure-x86-interpreter.symbol-engine :refer :all])

(def ^:dynamic *local-pgm-state* 0)

(defn initialize-stack
  []
  (let [size (:stack-size memory-information)]
    (vec (repeat size 0))))

(defn initialize-heap
  []
  (let [size (:heap-size memory-information)]
    (vec (repeat size 0))))

(defrecord program-state-type [computation
                               reg-map
                               flags
                               code
                               heap
                               stack
                               memory-allocations
                               mem-info
                               symbols-collection
                               ])

(def default-program-state (program-state-type. "nop"
                                                machine-register-map
                                                machine-flags
                                                ()
                                                {}
                                                {}
                                                {}
                                                memory-information
                                                []))

(defn get-rip
  ([] (-> *local-pgm-state* :reg-map :rip))
  ([pgm-state] (-> pgm-state :reg-map :rip)))

(defn get-code
  ([] (-> *local-pgm-state* :code))
  ([pgm-state] (-> pgm-state :code)))

(defn get-reg-map
  ([] (-> *local-pgm-state* :reg-map))
  ([pgm-state] (-> pgm-state :reg-map)))

(defn set-rip
  ([pgm-state new-value] (assoc-in pgm-state [:reg-map :rip] new-value)))

(defn set-computation
  ([pgm-state new-ip] (assoc pgm-state :computation (nth (get-code pgm-state) new-ip))))

(defn get-reg
  [reg]
  (let [[size start _] (get-bit-width reg)
        reg-keyword (keyword reg)
        alias (get alias-mapping reg-keyword)
        reg-map (get-reg-map)
        full-value (alias reg-map)]
    (extract-bits full-value size start)))

(defn set-reg
  [reg new-value]
  (let [[size start write-mask] (get-bit-width reg)
        extracted-value (extract-bits (parse-float new-value) size start)
        reg-keyword (keyword reg)
        alias (reg-keyword alias-mapping)
        reg-map (get-reg-map)
        old-value (get-reg alias)
        result (#(bit-or (unchecked-long %1) (bit-and (unchecked-long %2) (unchecked-long %3))) extracted-value write-mask old-value)]
    (assoc *local-pgm-state* :reg-map (assoc reg-map alias result))))

(defn read-heap
  [offset]
  (println "read-heap" offset))

(defn read-stack
  [offset]
  (println "read-stack" offset))

(defn write-heap
  [offset new-value]
  (println "write-heap" offset new-value))

(defn write-stack
  [offset new-value]
  (println "write-stack" offset new-value))

(defn call-instruction [nm args]
  (when-let [fun (ns-resolve *ns* (symbol nm))]
    (apply fun args)))

(defn memory-operation
  [operation offset new-value]
  (let [heap-limit (get-in *local-pgm-state* [:mem-info :heap-end])
        stack-begin (get-in *local-pgm-state* [:mem-info :stack-start])]
    (cond
      (< offset heap-limit) (call-instruction (str operation "-heap") (filter #(not= nil %) (seq [offset new-value])))
      (> offset stack-begin) (call-instruction (str operation "-stack") (filter #(not= nil %) (seq [offset new-value]))))))

(defn read-memory
  [offset]
  (memory-operation "read" offset nil))

(defn write-memory
  [offset new-value]
  (memory-operation "write" offset new-value))

(defn new-allocation
  [offset size]
  (assoc-in *local-pgm-state* [:memory-allocations] {offset size}))

(defn delete-allocation
  [offset]
  )

(defn add-symbol
  [name address size type]
  (def new-symbol (symbol-type. name
                                ""
                                address
                                size
                                type))
  (def with-symbols (assoc-in *local-pgm-state* [:symbols-collection] new-symbol))
  (assoc-in with-symbols [:heap]))