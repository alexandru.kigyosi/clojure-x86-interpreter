(ns clojure-x86-interpreter.core)
(require '[clojure-x86-interpreter.memory-instructions :refer :all])
(require '[clojure-x86-interpreter.program-state :refer :all])
(require '[clojure-x86-interpreter.utils :refer (extract-bits)])
(require '[clojure.string :as str])

(defn fetch
  [pgm-state]
  (def rip (get-rip pgm-state))
  (if (= (count (get-code pgm-state)) rip)
    (set-rip pgm-state 12345678900000)
    (let [new-pgm-state (set-computation pgm-state rip)]
      (set-rip new-pgm-state (+ rip 1)))))

(defn exec-instr
  [pgm-state]
  (binding [*local-pgm-state* pgm-state]
    (try
      (do (def vector-instr (str/split (:computation *local-pgm-state*) #" "))
          (def return-state (call-instruction (nth vector-instr 0) (rest vector-instr))))
      (catch Exception e (str "instruction not found: " (.toString e)))
      ))
  (if (= return-state nil)
    pgm-state
    return-state))

(defn main []
  (def initial-program-state (assoc-in default-program-state [:code] '("mov 8000655381 rax" "mov 123 ax")))
  (loop [program-state initial-program-state]
    (if (= 12345678900000 (get-rip program-state))
      program-state
      (let [evaluated-state (exec-instr (fetch program-state))]
        (println evaluated-state)
        (recur evaluated-state)))))

(main)